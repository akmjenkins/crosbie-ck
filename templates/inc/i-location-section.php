	<section class="nopad location-section primary-bg">
	
		<div class="location-section-title d-bg">
			<div class="sw">
			
				<h3 class="title">Visit Us at One of our Conveniently located offices.</h3>
			
				<div class="selector with-arrow">
					<select class="location-selector">
						<option>Select a Location</option>
						<option>Location One</option>
						<option>Location Two</option>
						<option>Location Three</option>
						<option>All Locations</option>
					</select>
					<span class="value">&nbsp;</span>
				</div><!-- .selector -->
			
			</div><!-- .sw -->
		</div><!-- .location-section-title -->
		
		<div class="gmap">
			<div 
				class="map" 
				data-center="0,0" 
				data-zoom="3"
				data-fit-bounds="true"
				data-markers='<?php 
					echo json_encode(
						array(
							array(
								'position' => '47.560786,-52.744331',
								'htmlmarker' => 'crosbie-map-marker',
								'title' => 'Location One',
								'address' => '1 Crosbie Pl St. Johns, NL A1B 3Y8',
								'phone' => '709.726.5414',
								'fax' => '709.123.4567',
								'link' => 'http://stackoverflow.com'
							),
							array(
								'position' => '47.52429,-52.772129',
								'htmlmarker' => 'crosbie-map-marker',
								'title' => 'Location Two',
								'address' => '1 Crosbie Pl St. Johns, NL A1B 3Y8',
								'phone' => '709.726.5414',
								'fax' => '709.123.4567',
								'link' => 'http://stackoverflow.com'
							),
							array(
								'position' => '47.5250616,-52.7900247',
								'htmlmarker' => 'crosbie-map-marker',
								'title' => 'Location Three',
								'address' => '1 Crosbie Pl St. Johns, NL A1B 3Y8',
								'phone' => '709.726.5414',
								'fax' => '709.123.4567',
								'link' => 'http://stackoverflow.com'
							)
						)
					); 
				?>'>
			</div>
		</div><!-- .gmap -->
		
	</section><!-- .location-section -->