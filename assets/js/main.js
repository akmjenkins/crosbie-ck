var ns = 'JAC-CROSBIE';
window[ns] = {};

// Utilities used pretty much everywhere 
// @codekit-append "scripts/debounce.js"
// @codekit-append "scripts/tests.js"
// @codekit-append "scripts/image.loader.js"

// Used in location.map.js
// @codekit-append "scripts/tim.microtemplate.js"
// @codekit-append "scripts/map/custom.infowindow.js"
// @codekit-append "scripts/map/html.marker.js"
// @codekit-append "scripts/gmap.js"

// @codekit-append "scripts/anchors.external.popup.js"
// @codekit-append "scripts/standard.accordion.js"
// @codekit-append "scripts/magnific.popup.js"
// @codekit-append "scripts/custom.select.js"
// @codekit-append "scripts/lazy.images.js"
// @codekit-append "scripts/tabs.js"
// @codekit-append "scripts/nav.js"
// @codekit-append "scripts/swiper.js"
// @codekit-append "scripts/preventOverScroll.js"
// @codekit-append "scripts/location.map.js"

// @codekit-append "global.js"